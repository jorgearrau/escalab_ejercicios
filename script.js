'use stric'
/** Ejercicio 1
 * 
 *  Escribir una funcion a la que se pasa como parametro un numero entero y devuelve como resultado un texto que 
 * indica si el numero es par o impar. Mostrar por pantalla el resultado devuelto por la funcion. 
 */

function mifuncion(x) {
    var res = x % 2;
    if(res == 0){
        console.log('El numero es par '+ x)
    }else {
        console.log('El numero es impar '+ x)
    }
}

mifuncion(61)

/** Ejercicio 2
 * Segun este string 'abcdefghijklmnñopqrstuvwxyz'. Hacer una funcion que recibe un parametro (un caracter), y
 * la funcion me debe devolver en que posicion en la cadena del string se encuentra el caracter entregado 
 * como argumento
 * 
 * ejemplo:
 *  posicion("e") //-> 4
 */

var abc = "abcdefghijklmnñopqrstuvwxyz"

function mifuncion2(e) {

    console.log(abc.indexOf(e));
    
}

mifuncion2("e") /** Resultado = 4 */


/** Ejercicio 3
 * Hacer una funcion a la que le paso un precio, y este me devuelve el precio con impuesto de 0.19%.
 * si yo ingreso 100 a la funcion, me deberia retornar con el formato "el precio con impuesto es de 1190"
 */

 var impuesto = 1.19;

 function miFuncion3(x) {
     result = x * impuesto;
     console.log("el precio con impuesto es de "+ result);
     
 }

miFuncion3(1000); /**Resultado: "el precio con impuesto es de 1190" */